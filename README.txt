
Usage:

var IrrParser = require('vvf-irr-parser');
var parser = new IrrParser( {
    // numberParalleleDownloads: // default 5,
    // numberParalleleImageParser: // default == numberParalleleDownloads,
    // startUrls: (Array) default: ['http://russia.irr.ru/real-estate/apartments-sale/search/list=list/page_len60/', 'http://russia.irr.ru/real-estate/rent/search/currency=RUR/list=list/page_len60/',
    // maxTres: default == 10
    // imagesFolder: default =='phones' - путь куда сохранять картинки с номерами телефонов

    // callbacks / events:
    // Вызов после парсинга одного объявления и загрузки всех его картинок с телефонами:
    // onParseOne / parseOne =function( error, url, html, array_paths_to_images, city, $)
    // onDone / done
    // - / error - при ошибке
    // - / lastPage - если была распарсена последняя страница в списке
    // - / restart - при ошибке загрузке - перед повтороной попыткой (повторная попытка через некоторое время)
} );
parser.run();