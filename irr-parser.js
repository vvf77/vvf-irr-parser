#!/usr/bin/node  --max-old-space-size=6000

'use strinct';
'jslint node: true';
/*jslint node: true */

var request = require('request');

var cookieJar = request.jar();
var userAgentString = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/36.0.1985.125 Chrome/36.0.1985.125 Safari/537.36';
request = request.defaults({ headers:{'User-Agent':userAgentString}, jar:cookieJar});
var cheerio = require('cheerio');
var async= require('async');
var fs = require('fs');
var EventEmitter = require("events").EventEmitter;
var util = require('util');
var path = require('path');

var log_debug = false;
var numberPagesToTest = null;

var default_config = {
    /* **** config ***** */
    numberParalleleDownloads: 5,
    numberParalleleImageParser: null,
    startUrls: [
        'http://russia.irr.ru/real-estate/apartments-sale/search/list=list/page_len60/',
        'http://russia.irr.ru/real-estate/rent/search/currency=RUR/list=list/page_len60/'
    ],
    maxTres:10,
    imagesFolder : 'phones',

    onParseOne: null,
    onImageReaded:null
    /* **** /config ***** */
};

var decode64=function( base64string ){ return new Buffer( base64string, 'base64' ).toString('utf8'); };

// Функция принимает задачу на загрузку и выполняет ее: просто запускает нужную функцию в зависмости от типа задачи. 
var worker = function( task, next ){
    task.tryNumber = 1+task.tryNumber||0;
    var self = this;
    // if error occured queue this task again, but not more than 15 times.
    var restartTask = function(){
        if( task.tryNumber > self.config.maxTres ){
            self.emit('error', 'errorLoad', task);
            if( log_debug ) console.log('Error load after 15 tries', task );
        }
        self.loaderQueue.push( task );
    };
    if( 'list' == task.type ){
        task.page = task.page || 1;
        self.irrParseList( task, function( err ){
            if( 'nopage' == err ){
                self.emit('lastPage', task );
                if(log_debug) console.log( 'Page %s was last. Do not continue lists load', task.page - 1);
            }else if( err ){
                if( log_debug ) console.log(' restart task by error', err);
                self.emit('restart', task.tryNumber, task );
                setTimeout( restartTask, 300*task.tryNumber+100);
            }else{
                task.tryNumber = 0;
                task.page++;
                self.loaderQueue.push( task );
            }
            setTimeout(next , 1 );
        } );
    }else{
        this.irrParseOne( task.url, function( err ){
            if( err ){
                self.emit('error',err, task);
                setTimeout(restartTask, 300*task.tryNumber+100);
            }
            setTimeout(next , 1 );
        } );
    }
};

var IRRParser = function( params ){
    // resultCallback, recogonizedImagesCallback
    var self = this;
    this.config = {};
    for(var i in default_config ){
        if( default_config.hasOwnProperty(i)){
            this.config[i] = default_config[i];
        }
    }
    util._extend( this.config, params );

    this.loaderQueue = async.queue(worker.bind( this ), this.config.numberParalleleDownloads);
    if( 'function' == typeof this.config.onParseOne ){
        self.on('parseOne', this.config.onParseOne);
    }
    if( this.config.onImageReaded || this.config.imageRecogonize){
        this.recogonizeImagesQueue = async.queue( imgParser.bind( this ), this.config.numberParalleleImageParser || this.config.numberParalleleDownloads);
        if( typeof this.config.onImageRead == 'function'){
            this.on('imageReaded', this.config.onImageReaded );
        }
    }
    this.count = 0;
    if( 'function' == typeof this.config.onDone){
        this.on('done', this.config.onDone);
    }
    this.loaderQueue.drain = function(){
        self.emit('done', self.count );
    };

    this.run = function(){
        self.config.startUrls.forEach( function(url){
            self.loaderQueue.push({ url:url, type:'list'});
        });
        for(var i in arguments) if(arguments.hasOwnProperty(i)){
            self.loaderQueue.push({ url:arguments[i], type:'list'});
        }
    };
};

util.inherits(IRRParser, EventEmitter);

IRRParser.prototype.irrParseList = function( task, next ){
    var self = this;
    if( log_debug ) console.log('List: ', task.url );
    request({ url: task.url + 'page'+task.page+'/' ,  jar:cookieJar, method:'GET'}, function( err, resp, body ){
        if( err ) return next( err );
        if( !body ) return next('Empty page');
        var $ = cheerio.load( body );
        var page = $('.adds_paging.left .current a').html();
        if( self.numberPagesToTest && page > self.numberPagesToTest) return next('nopage');
        // if don't need "hidden pages": 
        // if( task.page>1 && !page ) return  next('nopage');
        // if parse "hidden pages"
        if( task.page>1 && '1' == page ) return next('nopage');
        $('.main_props .add_title').each(function(i,a){
            self.loaderQueue.push({ url:$(a).attr('href'), type:'one' });
        });
        $= null;
        body=null;
        next();
    });
};

IRRParser.prototype.irrParseOne = function( url, next ){
    var self = this;
    var advUrl = url;
    //if( log_debug ) console.log('One : ', url );
    request({ url: url, jar:cookieJar, method:'GET'}, function( err, resp, body ){
        if(err){
            return next( err );
        }
        if( !body ) return next('Empty page');
        var $ = cheerio.load( body );
        var phonesImg64 = $('#allphones').val();
        if( !phonesImg64 ){
            if( log_debug ) console.log('Empty #allphones', url );
            // Здесь если нужно - вызвать resultCallback но в параметре "телефоны" - указать пустой массив либо null.
            self.emit('parseOne', null, url, body, null, city, $ );
            next();
            return;
        }
        var $phones = cheerio.load( decode64( phonesImg64 ) )('img');
        var restLoadImg = $phones.length;
        var imagesLoaded = [];
        var number=$('.number').html().substr(9);
        //if( log_debug ) console.log('adv number:', number );

        var city = $('.address_link').html(). split(',')[0];
        self.count ++ ;

        var imgLoader = function(url, imgPath, tryNumber){
            if( self.recogonizeImagesQueue ){
                self.recogonizeImagesQueue.push({url:url, number:number,parentUrl:advUrl});
            }
            // May be don't download, instead recogonize though http://www.i2ocr.com/
            var saveStream = fs.createWriteStream( imgPath );
            request({url:url, jar:cookieJar, method:'GET'}, function( err, resp ){
                // TODO: check responce code and may be content type header
                if(err){
                    if( tryNumber>self.config.maxTres ){ // TODO: to config
                        restLoadImg --;
                        if( !restLoadImg ) self.emit( 'error', err, task, url, body, imagesLoaded, city, $ );
                        return;
                    }
                    tryNumber = 1+tryNumber||0;
                    if( log_debug ) console.log(' retry image load');
                    setTimeout( function(){
                        imgLoader(url, imgPath, tryNumber);
                    }, 300*tryNumber+100 );
                    return;
                }
                if( log_debug ) console.log( 'img: ',url );
                imagesLoaded.push( imgPath );
                saveStream.on('finish', function(){
                    restLoadImg --;
                    if( !restLoadImg ) self.emit('parseOne', null, url, body, imagesLoaded, city, $ );
                });
            }).pipe( saveStream );
        };
        //if( log_debug ) console.log(' one, images: ', $phones.length );
        $phones.each( function( i, img ){
            var imgName = number + '_' + i;
            var ext = img.attribs.src.split('.').pop();
            var imgPath = path.join( self.config.imagesFolder || '', imgName +'.'+ext );
            imgLoader( img.attribs.src, imgPath );
        });
        next(); // continue load other pages in paralell with image loading
    });
};

var imgParser = function(task , next){
    var self = this;
    if( log_debug) console.log('parse url:' , task.url);
    request({ url:'http://www.i2ocr.com/process_form', method:'POST', form:{i2ocr_options:'url',i2ocr_uploadedfile:'', i2ocr_url: task.url, i2ocr_languages:'ru, rus'}}, function(err,resp, body){
        if( err ){ self.emit('imageReaded', err ); return next(); }
        if(!body){ self.emit('imageReaded','Empty responce'); return next(); }
        var lookup = true;
        body.split('\n').forEach( function(line){
            if(lookup && line.indexOf('#ocrTextBox')>0 ){
                lookup = false;
                var txt='--';
                var $=function(){ return {val:function(s){txt=s; return {show:function(){}};}};};
                try{
                eval( line );
                }catch(err){
                    self.emit('imageReaded', err );
                    return next();

                }
                self.emit('imageReaded', null, txt.trim(), task.number, task.parentUrl );
                next();
                if( log_debug)  console.log(txt.trim(), url);
            }
        });
    });
};

IRRParser.prototype.setDebugMode = function(){ log_debug = true; };

var tests={
    count:0,
    resultCallback: function( error, url, html, images, city, $ ){
        if( !html ){
            console.log('FAIL: html empty');
        }
        if( !images ){
            console.log('FAIL: image empty');
        }
        $=null;
        tests.count ++;
        //console.log( 'Saved phone images: ', images);
    },
    run: function(){
        var parser = new IRRParser({ onParse: tests.resultCallback });
        parser.numberPagesToTest = 1;
        parser.on('error', console.log )
            .on('restart', console.log )
            .run();
        //resultCallback = tests.resultCallback;
    }
};
var main=function(){
    tests.run();
};

if( !module.parent ){
    log_debug = true;
    //numberPagesToTest = 2;
    main();
}
module.exports = IRRParser;